
package Integral;

import java.util.Objects;

/**
 *
 * @author CORDOBA
 */
public class Proyecto {
    //Atributos
    private String id;
    private String nombre;
    private String monto;
    //Getter y Setter
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }
    //Metodos Constructor
    public Proyecto(String id, String nombre, String monto) {
        this.id = id;
        this.nombre = nombre;
        this.monto = monto;
    }

    public Proyecto() {
    }
    //Metodo Mostrar Datos
    public void mostrarDatos(){
        System.out.println(id + ": " + nombre + monto);
    }
}
