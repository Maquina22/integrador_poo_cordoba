
package Integral;

import java.util.Objects;

/**
 *
 * @author CORDOBA
 */
public class Agregar {
    //Atributos
    private String id;
    private String nombre;
    
    //Getter y Setter
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    //Metodos Constructor
    public Agregar(String id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Agregar() {
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.id);
        hash = 47 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Agregar other = (Agregar) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }
        
    //Metodo Mostrar Datos
    public void mostrarDatos(){
        System.out.println(id + ": " + nombre);
    }   
}
