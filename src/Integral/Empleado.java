
package Integral;

import java.util.Objects;

/**
 *
 * @author CORDOBA
 */
public class Empleado {
    //Atributos
    private String dni;
    private String ayn;
    private String telefono;
    private String email;
    //Getter y Setter
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getAyn() {
        return ayn;
    }

    public void setAyn(String ayn) {
        this.ayn = ayn;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    //Metodos Constructor
    public Empleado(String dni, String ayn, String telefono, String email) {
        this.dni = dni;
        this.ayn = ayn;
        this.telefono = telefono;
        this.email = email;
    }

    public Empleado() {
    }
    
    //Metodo Mostrar Datos
    public void mostrarDatos(){
        System.out.println(dni + ": " + ayn + telefono + email);
    }     
}
