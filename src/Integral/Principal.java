
package Integral;

import java.util.*;

/**
 *
 * @author CORDOBA
 */
public class Principal {
    
    static Scanner entrada = new Scanner(System.in); 
    static HashMap <String, Empleado> lista = new HashMap<>();
    static HashMap <String, Proyecto> lista1 = new HashMap<>();
    static Set <Agregar> agregar = new HashSet <Agregar>();
       
    public static void main(String[] args){        
        menuPrincipal();        
    }
    
    //Menu Principal
    public static void menuPrincipal(){
        //Menu Principal
        int opcion = 0;
        while(opcion < 3){
            try{
                System.out.println("");
                System.out.println("* MENU DE OPCIONES! *");
                System.out.println("1. Empleados");
                System.out.println("2. Proyectos");            
                System.out.println("3. Salir");
                System.out.println("");
                System.out.print("Elegir una opcion: ");
                opcion = entrada.nextInt();                
            }catch (InputMismatchException ex){
                
            }
            
            switch(opcion){
                //Menu List
                case 1: 
                        menuEmpleado();
                        break;
                //Menu Set        
                case 2: 
                        menuProyecto();
                        break;                
                //Salir Menu Principal
                case 3:
                        System.out.println("");
                        System.out.println("El programa finalizo exitosamente...");
                        break;
                        
                default: 
                        System.out.println("");
                        System.out.println("Error...   Opcion incorrecta!");
                        opcion = 0;
                        System.out.print("Enter para continuar...");
                        entrada.nextLine();
                        entrada.nextLine();
            }
        }
    }
    //Menu EMPLEADO
    public static void menuEmpleado(){
        int opcion2 = 0;
        while(opcion2 < 4){
            try{
                System.out.println("");
                System.out.println(" * * * MENU EMPLEADO!  * * *");
                System.out.println("1. Agregar empleado");
                System.out.println("2. Eliminar empleado");            
                System.out.println("3. Listar empleado");
                System.out.println("4. Volver al menu principal...");
                System.out.println("");
                System.out.print("Elegir una opcion: ");
                opcion2 = entrada.nextInt();
            }catch(InputMismatchException ex){
                
            }
            
            switch(opcion2){                
                case 1: //Agregar Empleado 
                        agregarEmpleado();                                     
                        break;
                        
                case 2: //Eliminar Empleado
                        eliminarEmpleado();                                                                       
                        break;                
    
                case 3: //Mostrar Empleado
                        mostrarEmpleado();                            
                        break;
   
                case 4: //Salir menu empleado
                        System.out.println("");
                        break;
                        
                default: 
                        System.out.println(" ");
                        System.out.println("Error...   Opcion incorrecta!");
                        opcion2 = 0;
                        System.out.print("Enter para continuar...");
                        entrada.nextLine();
                        entrada.nextLine();
            }
        }
    }
    //Menu PROYECTO
    public static void menuProyecto(){
        int opcion2 = 0;
        while(opcion2 < 8){
            try{
                System.out.println("");
                System.out.println(" * * * MENU PROYECTO!  * * *");
                System.out.println("1. Agregar nuevo proyecto");
                System.out.println("2. Eliminar proyecto");           
                System.out.println("3. Listar proyectos");
                System.out.println("4. Agergar empleado a un proyecto");
                System.out.println("5. Quitar empleado de un proyecto");
                System.out.println("6. Listar datos de un proyecto");
                System.out.println("7. Calcular total montos destinados a proyectos");
                System.out.println("8. Volver al menu principal...");
                System.out.println("");
                System.out.print("Elegir una opcion: ");
                opcion2 = entrada.nextInt();
            }catch(InputMismatchException ex){
                
            }
            
            switch(opcion2){                
                case 1: //Agregar nuevo proyecto 
                        agregarProyecto();                                     
                        break;
                        
                case 2: //Eliminar Proyecto
                        eliminarProyecto();                                                                       
                        break; 
    
                case 3: //Mostrar Proyectos
                        mostrarProyecto();                                        
                        break;
                        
                case 4: //Agergar empleado a un proyecto
                        agregarEmpleadoProyecto();
                        mostrarEmpleadoAgregado();
                        break;
                        
                case 5: //Quitar empleado de un proyecto
                        eliminarEmpleadoProyecto();                                       
                        break;        
                
                case 6: //Mostrar datos de un Proyectos
                        mostrarDatosProyecto();                                        
                        break;
                        
                case 7: //Calcular total montos destinados a proyectos
                        //calcularMonto();                                        
                        break;
                        
                case 8: //Salir de Set
                        System.out.println("");
                        break;
                        
                default: 
                        System.out.println(" ");
                        System.out.println("Error...   Opcion incorrecta!");
                        opcion2 = 0;
                        System.out.print("Enter para continuar...");
                        entrada.nextLine();
                        entrada.nextLine();
            }
        }
    }
    
    //Agregar empleado
    public static void agregarEmpleado(){
        System.out.println("");                                        
        int cantidad = 1;
        for(int i=0; i<cantidad; i++){
            entrada.nextLine();
            System.out.print("Ingresar DNI: ");
            String dni = entrada.nextLine();                                            
            System.out.print("Ingresar Apellido y Nombre: ");
            String ayn = entrada.nextLine();
            System.out.print("Ingresar telefono: ");
            String telefono = entrada.nextLine();   
            System.out.print("Ingresar email: ");
            String email = entrada.nextLine();            
            Empleado empleado = new Empleado(dni, ayn, telefono, email);
            lista.put(dni, empleado);
            System.out.println("");
            System.out.println("Empleado agregado correctamente!");
            System.out.println("");
            System.out.print("Enter para continuar...");                                            
            entrada.nextLine();
        }
    }
    
    //Eliminar empleado
    public static void eliminarEmpleado(){        
        System.out.println("");
        System.out.print("Igresar dni del empleado que desea eliminar: ");
        String eliminar = entrada.next();
        if(lista.containsKey(eliminar)){
            lista.remove(eliminar);
            System.out.println("");
            System.out.println("Empleado eliminado correctamente!");
        }else{
            System.out.println("");
            System.out.println("No existe!");
        }                                            
        System.out.println("");
        System.out.print("Enter para continuar...");
        entrada.nextLine();
        entrada.nextLine();
    }
    
    //Mostrar empleado
    public static void mostrarEmpleado(){
        System.out.println("");
        int cont = 0, acu = 0;
        System.out.println(" * * * LISTA EMPLEADOS * * *" );
        for(Map.Entry <String, Empleado> entry : lista.entrySet()){
            acu = acu +1;                                            
            System.out.println(" ");
            System.out.println(acu + "). \t " + entry.getValue().getDni() + "\t " + entry.getValue().getAyn() + "\t " + entry.getValue().getTelefono() + "\t " + entry.getValue().getEmail()); 
            cont = cont + 1;                                         
        }
        System.out.println("");
        System.out.println("Total de empleados: " + cont);
        System.out.println(" ");
        System.out.print("Enter para continuar...");
        entrada.nextLine();
        entrada.nextLine();
    }    
      
    
    
    
    
    //Agregar nuevo proyecto
    public static void agregarProyecto(){
        System.out.println("");                                        
        int cantidad = 1;
        for(int i=0; i<cantidad; i++){
            entrada.nextLine();
            System.out.print("Ingresar ID: ");
            String id = entrada.nextLine();                                            
            System.out.print("Ingresar nombre del proyecto: ");
            String nombre = entrada.nextLine();
            System.out.print("Ingresar monto destinado al proyecto: ");
            String monto = entrada.nextLine();                        
            Proyecto proyecto = new Proyecto(id, nombre, monto);
            lista1.put(id, proyecto);
            System.out.println("");
            System.out.println("Proyecto agregado correctamente!");
            System.out.println("");
            System.out.print("Enter para continuar...");                                            
            entrada.nextLine();
        }
    }
    
    //Eliminar proyecto
    public static void eliminarProyecto(){        
        System.out.println("");
        System.out.print("Igresar id del proyecto que desea eliminar: ");
        String eliminar = entrada.next();
        if(lista1.containsKey(eliminar)){
            lista1.remove(eliminar);
            System.out.println("");
            System.out.println("Proyecto eliminado correctamente!");
        }else{
            System.out.println("");
            System.out.println("No existe!");
        }                                            
        System.out.println("");
        System.out.print("Enter para continuar...");
        entrada.nextLine();
        entrada.nextLine();
    }
    
    //Mostrar proyecto
    public static void mostrarProyecto(){
        System.out.println("");
        int cont = 0, acu = 0;
        System.out.println("Orden \t\t ID \t\t Nombre del Proyecto \t\t Monto destinado" );
        for(Map.Entry <String, Proyecto> entry : lista1.entrySet()){
            acu = acu +1;                                            
            System.out.println(" ");
            System.out.println(acu + "). \t\t " + entry.getValue().getId() + "\t\t " + entry.getValue().getNombre() + "\t\t\t " + entry.getValue().getMonto()); 
            cont = cont + 1;                                         
        }
        System.out.println("");
        System.out.println("Total de proyectos: " + cont);
        System.out.println(" ");
        System.out.print("Enter para continuar...");
        entrada.nextLine();
        entrada.nextLine();
    }  
    
    //buscar empleado
    public static String buscarEmpleado(){
        String encontrado;
        System.out.println("");
        System.out.print("Igresar dni del empleado que desea asignar al proyecto: ");
        String buscar = entrada.next();
        if(lista.containsKey(buscar)){
            System.out.println("");
            encontrado = lista.get(buscar).getAyn();
        }else{
            System.out.println("");
            encontrado = "No existe!";
            
        }
        System.out.println("");
        System.out.print("Enter para continuar...");
        entrada.nextLine();
        entrada.nextLine();
        return encontrado;
    }
    
    //buscar proyecto
    public static String buscarProyecto(){
        String encontrado;
        System.out.println("");
        System.out.print("Igresar id del proyecto al que desea agregar un emplado: ");
        String buscar1 = entrada.next();
        if(lista1.containsKey(buscar1)){
            System.out.println("");
            encontrado = lista1.get(buscar1).getId();
        }else{
            System.out.println("");
            encontrado = "No existe!";
        }
        System.out.println("");
        System.out.print("Enter para continuar...");
        entrada.nextLine();
        entrada.nextLine();
        return encontrado;
    }
    
    //Agregar empleado a proyecto
    public static void agregarEmpleadoProyecto(){        
        int cantidad = 1;      
        entrada.nextLine();
        
        for(int i=0; i<cantidad; i++){                                
            Agregar emp = new Agregar();
            emp.setId(buscarEmpleado());
            emp.setNombre(buscarProyecto());
            agregar.add(emp);           
            System.out.println("Empleado agregado al proyecto correctamente!");
            System.out.println("");
            System.out.print("Enter para continuar...");                                            
            entrada.nextLine();
        }
    }
    
    //Mostrar empleado agregado al proyecto
    public static void mostrarEmpleadoAgregado(){
        System.out.println(" ");
        int cont = 0, acu = 0;
        System.out.println("Orden \t\t ID \t\t Empleado");
        for(Agregar i : agregar){
            acu = acu +1;                                            
            System.out.println(" ");
            System.out.println( acu + "). \t\t " + i.getNombre()  + "\t\t " + i.getId());  
            cont = cont + 1;
        }
        System.out.println(" ");
        System.out.println("Total de estudiantes: " + cont);
        System.out.println(" ");
        System.out.print("Enter para continuar...");
        entrada.nextLine();
    }
    
    //Quitar empleado de un proyecto   
    public static void eliminarEmpleadoProyecto(){  
        boolean bandera = true;
        System.out.println(" ");
        System.out.print("Ingresar Id de Poryecto desde donde desea eliminar: ");
        entrada.nextLine();
        String buscarId = entrada.next();
        System.out.print("Ingresar apellido y nombre del empleado que desea eliminar: ");
        entrada.nextLine();
        String buscarNom = entrada.next();
        Agregar agrega = new Agregar();
        for(Agregar i : agregar){
            if(i.getId().equals(buscarId) && i.getNombre().equals(buscarNom)){
                agrega = i;
                bandera = false;
                System.out.println("Empleado eliminado correctamente!");
            }
        }
        agregar.remove(agrega); 
        if(bandera == true){
            System.out.println("");
            System.out.println("No existe!");
        }
        System.out.println("");
        System.out.print("Enter para continuar...");
        entrada.nextLine();
        entrada.nextLine();        
    }
    
    //Listar datos de un proyecto
    public static void mostrarDatosProyecto() {
        int cont = 0;
        System.out.print("Igresar apellido y nombre del empleado que desea eliminar: ");
        String auxid =  entrada.nextLine();
        
        for(Agregar a: agregar){
            if(auxid == a.getId()){
                System.out.println("\t "+a.getId()+"\t"+a.getNombre());
                cont++;
            }
        }
        System.out.println("El total de Empleados en el Proyecto es: "+cont);
    }
    
}  




   